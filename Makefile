export PROTO_GO_PATH := interfaces
export PROTO_TS_PATH := ts
export PROTO_PATH := proto

help:
	@echo "See Makefile"

### Docker Generated
generate-docker: generate-go-docker generate-ts-docker

generate-go-docker:
	@mkdir -p $$HOME/go/docker/cache/go-build && chmod -R 777 $$HOME/go/docker/cache/go-build
	@docker container run --init -t -i --rm \
	-v $$HOME/go/docker:/go \
	-v $$(pwd):/data \
	-u $$(id -u ${USER}):$$(id -g ${USER}) \
	-w /data \
	afisme/golang:1.20 make generate-go
	@echo "Done"

generate-ts-docker: export COMMAND=make generate-ts
generate-ts-docker: node-docker-run

### For Golang Generator
generate-go: generate-proto-golang protoc-go-inject-tag
generate-proto-golang:
	@echo "Generating proto file for golang and javascript web"
	@echo "Using protoc version" $$(protoc --version), "protoc-gen-go version", $$(protoc-gen-go --version)
	@echo "Generate go struct to" $$(pwd)/$$PROTO_GO_PATH
	@rm -rf $$(pwd)/$$PROTO_GO_PATH && mkdir -p $$(pwd)/$$PROTO_GO_PATH
	@for entry in $$(find $$PROTO_PATH -iname "*.proto"); do\
		protoc --go_out=$$(pwd)/$$PROTO_GO_PATH \
		--go_opt=paths=import \
		--go_opt=module=code.afis.me/proto/modules/listener/interfaces \
		--go-grpc_out=$$(pwd)/$$PROTO_GO_PATH \
		--go-grpc_opt=paths=import \
		--go-grpc_opt=module=code.afis.me/proto/modules/listener/interfaces \
		$$entry;\
    done
 protoc-go-inject-tag:
	@echo "Generating protoc-go-inject-tag for golang"
	@for entry in $$(find $$PROTOGOPPATH -iname "*.go"); do\
		protoc-go-inject-tag -input=$$entry;\
		sed -i '/\/\/ versions:/d' $$entry && sed -i '/\/\/ 	protoc/d' $$entry;\
	done


### Generated For TypeScript
generate-ts:
	@echo "Generating proto file typescript ...."
	@echo "Generate typescript to" $$PROTO_TS_PATH
	@rm -rf $$PROTO_TS_PATH && mkdir -p $$PROTO_TS_PATH
	@yarn install
	@for entry in $$(find $$PROTO_PATH -iname "*.proto"); do\
		npx protoc --ts_out $$PROTO_TS_PATH --proto_path=$$(pwd)/proto/  "$$(basename $$entry)";\
	done




node-docker-run: node-docker-prepare
	@docker container run --init -t -i --name node-docker-proto-dev --rm \
	-v $$(pwd)/$$DIR:/data \
	-v $$HOME/.netrc:/home/node/.netrc:ro \
	-v $$HOME/.cache/docker/node/cache:/home/node/.cache \
	-v $$HOME/.cache/docker/node/yarn/.yarnrc:/home/node/.yarnrc \
	-v $$HOME/.cache/docker/node/npm:/home/node/.npm \
	-u $$(id -u ${USER}):$$(id -g ${USER}) \
	-e COMMAND="$$COMMAND" \
	-w /data \
	afisme/node:18 $$COMMAND

node-docker-prepare:
	@mkdir -p $$HOME/.cache/docker/node/cache
	@mkdir -p $$HOME/.cache/docker/node/yarn
	@mkdir -p $$HOME/.cache/docker/node/npm
	@touch $$HOME/.cache/docker/node/yarn/.yarnrc
